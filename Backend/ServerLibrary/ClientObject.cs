using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using GameLibrary;

namespace ServerLibrary
{
    public class ClientObject
    {
        List<ClientObject> onlinePlayers = new List<ClientObject>();
        private const int KBytes = 10;
        public static ServerAlgo.HPassword hashPassword = new ServerAlgo.HPassword();
        internal PlayerData player = null;
        public static string GameVersion = "0.0.1";
        public TcpClient client;
        public NetworkStream stream;
        public DataBaseConnector dataBaseConnector;
        public DataBaseConnectorGame dataBaseConnectorGame;
        public DataBaseUpdate dataBaseUpdate;

        public ClientObject(TcpClient tcpClient, DataBaseConnector dataBaseConnector, List<ClientObject> onlinePlayers, DataBaseConnectorGame dataBaseConnectorGame, DataBaseUpdate dataBaseUpdate)
        {
            client = tcpClient;
            stream = client.GetStream();
            this.dataBaseConnector = dataBaseConnector;
            this.onlinePlayers = onlinePlayers;
            this.dataBaseConnectorGame = dataBaseConnectorGame;
        }

        public void Process()
        {
            NetworkStream stream = null;
            try
            {
                stream = client.GetStream();
                byte[] data = new byte[2048]; // буфер для получаемых данных
                while ( true )
                {
                    byte[] message = GetMessage();
                    if ( message.Length <= 0 )
                        throw new Exception( "" );
                    else
                    {
                        MessageController( ( RequestInfo )RequestInfo.Deserialize( message ) );
                    }

                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.Message );
                Console.WriteLine( ex.StackTrace );
            }
            finally
            {
                if ( stream != null )
                    stream.Close();
                if ( client != null )
                    client.Close();
                if ( this.player != null && onlinePlayers.Contains( this ) )
                    onlinePlayers.Remove( this );
            }
        }
        // чтение входящего сообщения и преобразование в строку
        protected byte[] GetMessage()
        {
            byte[] data = new byte[1024* KBytes]; // буфер для получаемых данных
            lock (data)
            {
                int bytes = 0;
                do
                {
                    bytes = stream.Read(data, bytes, data.Length - bytes);
                }
                while (stream.DataAvailable);

                //Если приходит пустое сообщение то удаляем Connection 
                if (bytes <= 0)
                {
                    throw new Exception("User isn't active");
                }
            }
            return data;
        }

        public void SendMessage(DataInfo message)
        {
            byte[] data = message.Serialize();
            if (data.Length >= KBytes * 1024)
            {
                Console.WriteLine("To long message (SendMessage ClientObj)" + message.GetType());
            }
            lock (data)
            {
                try
                {
                    this.stream.Write(data, 0, data.Length);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }
        }

        public void MessageController(RequestInfo request)
        {
            try
            {

                switch (request.requestType)
                {

                    case RequestInfo.RequestType.Autorization:
                        AutorizationRequest autorization = (AutorizationRequest)request;
                        Authorization(autorization);
                        SendMainInfo();
                        break;
                    case RequestInfo.RequestType.Registration:
                        RegistrationRequest registration = (RegistrationRequest)request;
                        Registration(registration);
                        SendMainInfo();
                        break;
                    case RequestInfo.RequestType.GetById:
                        GetByIdRequest get = (GetByIdRequest)request;
                        switch (get.getByIdType)
                        {
                            case GetByIdRequest.GetById.Event:
                                //Получить событие по айди get.id
                                //getEvent(get);
                                break;
                            case GetByIdRequest.GetById.User:
                                //Получить пользователя по айди get.id
                                GetPlayer(get);
                                break;
                            case GetByIdRequest.GetById.RegisteredEvents:
                                RegisteredEvents(get);
                                break;
                            case GetByIdRequest.GetById.Reviews:
                                GetReviews(get);
                                break;
                            case GetByIdRequest.GetById.CastleReviews:
                                GetCastleReviews(get);
                                break;
                            case GetByIdRequest.GetById.Castle:
                                GetCastleById(get);
                                break;
                            case GetByIdRequest.GetById.CollectMoney:
                                CollectMoney(get);
                                break;
                            case GetByIdRequest.GetById.RequestString:
                                RequestString requestString = (RequestString)get;
                                switch (requestString.requestStringType)
                                {
                                    case RequestString.RequestStringType.ChangeUserDescription:
                                        ChangeUserDescription(requestString);
                                        break;
                                    case RequestString.RequestStringType.AddEventReview:
                                        AddEventReview(requestString);
                                        break;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case RequestInfo.RequestType.PeopleByName:
                        RequestPeople req = (RequestPeople)request;
                        UsersByNicknames(req);
                        break;
                    case RequestInfo.RequestType.ChangeSubscription:
                        RequestChangeSubscription requestChangeSubscription = (RequestChangeSubscription)request;
                        switch (requestChangeSubscription.subscriptionType)
                        {
                            case RequestChangeSubscription.SubscriptionType.deleteUserFromEvent:
                                ChangeEventSubscription(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.addUserToEvent:
                                ChangeEventSubscription(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.exitFromCastle:
                                ExitFromCastle(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.joinToCastle:
                                EnterToCastle(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.makeAColeader:
                                MakeAColeader(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.changeLeader:
                                //Добавить сделать пользователя главой отправить новый ResponseCastle
                                ChangeLeader(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.unmakeAColeader:
                                UnMakeAColeader(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.buyItem:
                                BuyItem(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.donateToCastle:
                                DonateToCastle(requestChangeSubscription);
                                break;
                            case RequestChangeSubscription.SubscriptionType.updateBuilding:
                                UpdateBuilding(requestChangeSubscription);
                                break;
                        }

                        break;
                    case RequestInfo.RequestType.AddEvent:
                        RequestAddEvent requestAddEvent = (RequestAddEvent)request;
                        AddEvent(requestAddEvent);
                        break;
                    case RequestInfo.RequestType.FutureEvents:
                        FutureEvents(player.Id);
                        break;
                    case RequestInfo.RequestType.FirstPlayers:
                        FirstPlayers();
                        break;
                    case RequestInfo.RequestType.MainInfo:
                        SendMainInfo();
                        break;
                    case RequestInfo.RequestType.CreateArmy:
                        CreateArmy((RequestCreateArmy)request);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.StackTrace);
            }
            finally
            {
                if (player != null)
                    Console.WriteLine(player.NickName + " " + request.GetType());
                else
                    Console.WriteLine(request.GetType());
            }
        }
        public void AddEventReview(RequestString requestString)
        {
            player = dataBaseConnector.getPlayerById( player.Id );
            DateTime dt = DateTime.Now;
            ReviewData rd = new ReviewData( player.Id, player.NickName, requestString.String, dt );
            dataBaseConnector.AddReview( player.Id, requestString.id, requestString.String, dt );
            SendMessage(new ResponseUpdate(new ResponseReviews(new List<ReviewData>{ rd}, ResponseReviews.ResponseReviewsType.EventReviews ), ResponseUpdate.ResponseUpdateType.UpdateEventReviews));
        }
        public void CollectMoney(GetByIdRequest request)
        {
            player = dataBaseConnector.getPlayerById( request.id );
            BuildingInfo market = dataBaseConnectorGame.GetBuildings( player.castleId )[2];
            int moneyToAdd = BuildingManager.MoneyToCollect(player.collectDate,market);
            player.collectDate = DateTime.Now;

            dataBaseConnectorGame.AddMoneyToCastle( player.castleId, 0, moneyToAdd );

            dataBaseConnectorGame.UpdateUser( player );

            UpdateCastleReview( new List<ReviewData> {
                new ReviewData( player.castleId, player.NickName, "Собирает на рынке " + moneyToAdd + " монет!", DateTime.Now )} );
        }
        public void UpdateBuilding(RequestChangeSubscription requestChangeSubscription)
        {
            player = dataBaseConnector.getPlayerById( player.Id );
            CastleInfo castle = dataBaseConnectorGame.GetCastle( player.castleId );
            BuildingInfo building = castle.buildings[requestChangeSubscription.userId];
            if (BuildingManager.IsEventMoneyBuilding(building) && castle.eventMoney >= requestChangeSubscription.targetId)
            {
                dataBaseConnectorGame.AddMoneyToCastle(castle.castleID,-requestChangeSubscription.targetId,0);
                building.introducedResources += requestChangeSubscription.targetId;
                building = BuildingManager.GetNewBuilding( building );
                dataBaseConnectorGame.UpdateBuilding( castle.castleID, building );
                UpdateCastleReview( new List<ReviewData> {
                new ReviewData( player.castleId, player.NickName, "Вложил в " + building.name+" " + requestChangeSubscription.targetId + " монет!", DateTime.Now )} );
            }
            else
                if ( !BuildingManager.IsEventMoneyBuilding( building ) && castle.marketMoney >= requestChangeSubscription.targetId )
                {
                    dataBaseConnectorGame.AddMoneyToCastle( castle.castleID, 0, -requestChangeSubscription.targetId );
                    building.introducedResources += requestChangeSubscription.targetId;
                    building = BuildingManager.GetNewBuilding( building );
                    dataBaseConnectorGame.UpdateBuilding( castle.castleID, building );
                    UpdateCastleReview( new List<ReviewData> {
                 new ReviewData( player.castleId, player.NickName, "Вложил в " + building.name+" " + requestChangeSubscription.targetId + " монет!", DateTime.Now )} );
                }
        }
        public void DonateToCastle(RequestChangeSubscription requestChangeSubscription)
        {
            player = dataBaseConnector.getPlayerById( player.Id );
            if (requestChangeSubscription.targetId <= player.Money )
            {
                dataBaseConnectorGame.AddMoneyToCastle(requestChangeSubscription.userId,requestChangeSubscription.targetId,0);
                player.Money -= requestChangeSubscription.targetId;
                dataBaseConnectorGame.UpdateUser(player);
                UpdateCastleReview( new List<ReviewData> { 
                new ReviewData( requestChangeSubscription.userId, player.NickName, "Пожертвовал в казну " + requestChangeSubscription.targetId + " монет!", DateTime.Now )} );

                SendMessage( new ResponseUpdate( new ResponseUser( ResponseUser.ResponseUserType.PurchaseOk, player ) , ResponseUpdate.ResponseUpdateType.UpdatePlayer) );

            }
        }

        public void ChangeUserDescription(RequestString requestString)
        {
            dataBaseConnector.UpdateDescriptionUser(requestString.id, requestString.String);
            player = dataBaseConnector.getPlayerById(requestString.id);
            SendMessage(new ResponseUser( ResponseUser.ResponseUserType.DescriptionUpdate, player));
        }
        public void ChangeLeader(RequestChangeSubscription requestChangeSubscription)
        {
            dataBaseConnectorGame.RemoveLeader(requestChangeSubscription.targetId );
            dataBaseConnectorGame.SetLeader(requestChangeSubscription.userId, requestChangeSubscription.targetId);
            UpdateCastleReview( new List<ReviewData> { new ReviewData( player.Id, player.NickName, "Снимает с себя княжеские полномочия!", DateTime.Now ),
            new ReviewData( requestChangeSubscription.targetId, player.NickName, "Становится новым князем!", DateTime.Now )} );
        }
        public void UnMakeAColeader(RequestChangeSubscription requestChangeSubscription)
        {
            PlayerData unMakingPerson = dataBaseConnector.getPlayerById(requestChangeSubscription.userId);
            dataBaseConnectorGame.SetColeaderPosition( requestChangeSubscription.userId, requestChangeSubscription.targetId, false );
            UpdateCastleReview(new List<ReviewData> { new ReviewData(requestChangeSubscription.targetId,player.NickName,"Понижает игрока " + unMakingPerson.NickName + " до солдата!",DateTime.Now)} );
        }
        public void MakeAColeader(RequestChangeSubscription requestChangeSubscription)
        {
            PlayerData MakingPerson = dataBaseConnector.getPlayerById( requestChangeSubscription.userId );
            dataBaseConnectorGame.SetColeaderPosition( requestChangeSubscription.userId, requestChangeSubscription.targetId, true );
            UpdateCastleReview(new List<ReviewData> { new ReviewData(requestChangeSubscription.targetId,player.NickName,"Повышает игрока " + MakingPerson.NickName + " до полководца!",DateTime.Now)} );
        }

        public void ExitFromCastle(RequestChangeSubscription requestChangeSubscription)
        {
            dataBaseConnectorGame.LeaveCastle( requestChangeSubscription.userId, requestChangeSubscription.targetId );
            
            string mess;
            string name;
            if ( requestChangeSubscription.userId == player.Id )
            {
                mess = "Выходит из замка!";
                name = player.NickName;
            }
            else
            {
                PlayerData leavingPers = dataBaseConnector.getPlayerById( requestChangeSubscription.userId );
                mess = "Был выгнан из замка игроком " + player.NickName + "!";
                name = leavingPers.NickName;
            }
            UpdateCastleReview( new List<ReviewData> { new ReviewData( requestChangeSubscription.targetId, name, mess, DateTime.Now ) } );
            SendMessage( new ResponseInfo( ResponseInfo.ResponseType.OkLeaveCastle ) );
        }
        public void EnterToCastle(RequestChangeSubscription requestChangeSubscription)
        {
            dataBaseConnectorGame.EnterToCastle( requestChangeSubscription.userId, requestChangeSubscription.targetId );
            UpdateCastleReview( new List<ReviewData> { new ReviewData( requestChangeSubscription.targetId, player.NickName, "Вступает в замок!", DateTime.Now ) } );
            SendMessage( new ResponseInfo( ResponseInfo.ResponseType.OkEnterCastle ) );
        }
        public void UpdateCastleReview( List<ReviewData> reviews){
            for ( int i = 0 ; i < reviews.Count ; i++ )
            {
                dataBaseConnectorGame.AddGameEvent(reviews[i]);
            }            
            var updating = new ResponseUpdate(new ResponseReviews(reviews, ResponseReviews.ResponseReviewsType.CastleReviews), ResponseUpdate.ResponseUpdateType.UpdateCastleReviews);
            foreach ( var client in onlinePlayers )
            {
                if (client.player != null )
                {
                    client.SendMessage( updating );
                }
            }
        }
        public void GetCastleById( GetByIdRequest get )
        {
            ResponseCastle responseCastle = new ResponseCastle( dataBaseConnectorGame.GetCastle( get.id ));
            SendMessage( responseCastle );
        }
        public void BuyItem(RequestChangeSubscription requestChangeSubscription)
        {
            if (player.Id == requestChangeSubscription.userId )
            {
                player = dataBaseConnector.getPlayerById( player.Id );
                Item targetItem = ItemList.GetItemByID( requestChangeSubscription.targetId );
                if (player.Money >= targetItem.ItemPower.Price )
                {
                    long WornItems = ItemList.PutItem( targetItem.Id, player.WornItems );
                    long Items = 0;
                    int Money = player.Money - targetItem.ItemPower.Price;
                    player = new PlayerData(Items,WornItems,player.NickName,player.Rating,Money,player.Id,player.Email,player.castleId,player.Description,player.isBanned,player.collectDate);
                    dataBaseConnectorGame.UpdateUser(player);
                    SendMessage(new ResponseUser(ResponseUser.ResponseUserType.PurchaseOk, player));
                }
                else
                {
                    SendMessage( new ResponseUser( ResponseUser.ResponseUserType.PurchaseReject , player ));
                }
            }
        }
        public void GetCastleReviews(GetByIdRequest get)
        {
            List<ReviewData> castleMessages = dataBaseConnectorGame.GetGameEvents(get.id);
            ResponseReviews response = new ResponseReviews( castleMessages, ResponseReviews.ResponseReviewsType.CastleReviews );
            SendMessage( response );
            //TODO
        }
        public void CreateArmy(RequestCreateArmy requestCreateArmy)
        {
            //Получить замок по id замка
            //Проверить все данные
            //Если цена армии меньше чем бабок в замке, то атаковать
            PlayerData leader = dataBaseConnector.getPlayerById( requestCreateArmy.userId );
            List<PlayerData> soldiers = new List<PlayerData>();
            for ( int i = 0 ; i < requestCreateArmy.soldiers.Count ; i++ )
            {
                soldiers.Add(dataBaseConnector.getPlayerById(requestCreateArmy.soldiers[i]));
            }
            Army army = new Army(soldiers,leader,requestCreateArmy.targetCastle);
            
            
            //TODO
        }
        public void SendMainInfo()
        {
            ResponseMainInfo responseMainInfo = new ResponseMainInfo( DateTime.Now, GameVersion, true, "" );
            SendMessage( responseMainInfo );
        }
        public void FirstPlayers()
        {
            ResponseUsers responseUsers = new ResponseUsers(dataBaseConnector.GetPlayersRaiting(), ResponseUsers.UsersType.Rating);
            SendMessage( responseUsers );
        }
        public void RegisteredEvents(GetByIdRequest request)
        {
            ResponseEvent events = new ResponseEvent(dataBaseConnector.GetMyRegisteredEvents(request.id));
            SendMessage(events);
        }
        public void GetReviews(GetByIdRequest request)
        {
            ResponseReviews reviews = new ResponseReviews(dataBaseConnector.GetReviews(request.id), ResponseReviews.ResponseReviewsType.EventReviews);
            SendMessage(reviews);
        }
        public void FutureEvents(int id)
        {
            ResponseUserEvent events = new ResponseUserEvent(dataBaseConnector.GetFutureEvents(id));
            SendMessage(events); 
        }
        public void AddEvent(RequestAddEvent request)
        {
            dataBaseConnector.AddEventData(request.eventData);
        }
        public void UsersByNicknames(RequestPeople request)
        {
            ResponseInfo response = new ResponseInfo(ResponseInfo.ResponseType.Error);
            response = new ResponseUsers(dataBaseConnector.GetUsersByNicknameSubstring(request.substring), ResponseUsers.UsersType.Name);
            SendMessage(response);
        }
        
        public void ChangeEventSubscription(RequestChangeSubscription request)
        {
            if (request.subscriptionType == RequestChangeSubscription.SubscriptionType.addUserToEvent)
            {
                //Добавить пользователя к событию
                dataBaseConnector.AddMyEvent(request.userId, request.targetId);
            }
            else
            {
                //Удалить пользователя из события
                dataBaseConnector.DeleteMyEvent(request.userId, request.targetId);
            }
        }
        public void GetPlayer(GetByIdRequest get)
        {
            PlayerData playerData = dataBaseConnector.getPlayerById(get.id);
            ResponseInfo response = new ResponseInfo(ResponseInfo.ResponseType.Error);
            if (playerData == null)
            {
                SendMessage(response);
            }
            else
            {
                response = new ResponseUser(ResponseUser.ResponseUserType.GetById, playerData);
                SendMessage(response);
            }
        }

        public void Authorization(AutorizationRequest autorization)
        {
            //string password = ServerAlgo.HPassword.GetPassword( autorization.Password );//hash
            hashPassword = new ServerAlgo.HPassword();
            string password = hashPassword.GetPassword(autorization.Password);
            int res = dataBaseConnector.LogIn(autorization.EMail, password );
            
            ResponseInfo response = new ResponseInfo(ResponseInfo.ResponseType.Error);
            if (res > 0)
            {
                PlayerData playerData = dataBaseConnector.getPlayerById(res);
                this.player = playerData;
                response = new ResponseUser(ResponseUser.ResponseUserType.Authorization, playerData);
            }
            else if (res == -1)
            {
                response = new ResponseInfo(ResponseInfo.ResponseType.AutorizationFalied_WrongLogin);
            }
            else if (res == -2)
            {
                response = new ResponseInfo(ResponseInfo.ResponseType.AutorizationFailed_WrongPassword);
            }
            SendMessage(response);
        }

        public void Registration(RegistrationRequest registration)
        {
            hashPassword = new ServerAlgo.HPassword();
            string password = hashPassword.GetPassword(registration.Password);
            int res = dataBaseConnector.SignUp(registration.EMail,registration.NickName, password );
            ResponseInfo response = new ResponseInfo(ResponseInfo.ResponseType.Error);
            if (res > 0)
            {
                PlayerData playerData = dataBaseConnector.getPlayerById(res);
                response = new ResponseUser(ResponseUser.ResponseUserType.Authorization, playerData);
            }
            else if (res == -1)
            {
                response = new ResponseInfo(ResponseInfo.ResponseType.RegistartionFailed_ExistLogin);
                SendMessage(response);
            }
            else if(res == -2)
            {
                response = new ResponseInfo(ResponseInfo.ResponseType.RegistrationFailed_ExistNickname);
                SendMessage(response);
            }
            SendMessage(response);
        }
        
    }

}
