﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary;

namespace ServerLibrary
{
    public class DataBaseConnector
    {
        //изменить строку подключения
        const string connectionString = @"Data Source=PwCGamification;Initial Catalog=PwC_Gamification;Integrated Security=True";

        private bool ExistSameLogin(ref SqlConnection sqlConnection, string login)
        {
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [Users] WHERE [Login] = @Login", sqlConnection);
            command.Parameters.AddWithValue("Login", login);
            sqlReader = command.ExecuteReader();
            int count = 0;
            try
            {
                if (sqlReader.Read())
                {
                    if (int.TryParse(sqlReader[0].ToString(), out count) && count != 0)
                    {
                        return true;
                    }
                    return false;
                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
        }

        private bool ExistSameNickname(ref SqlConnection sqlConnection, string nickname)
        {
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [Users] WHERE [Nickname] = @Nickname", sqlConnection);
            command.Parameters.AddWithValue("Nickname", nickname);
            sqlReader = command.ExecuteReader();
            int count = 0;
            try
            {
                if (sqlReader.Read())
                {
                    if (int.TryParse(sqlReader[0].ToString(), out count) && count != 0)
                    {
                        return true;
                    }
                    return false;
                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
        }

        private int GetCountUsers(ref SqlConnection sqlConnection)
        {
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [Users]", sqlConnection);
            sqlReader = command.ExecuteReader();
            int count = 0;
            try
            {
                if (sqlReader.Read())
                {
                    count = int.Parse(sqlReader[0].ToString());
                }
                return count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }

        }

        public int SignUp(string login, string nickname, string password)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;

            if (ExistSameLogin(ref sqlConnection, login))
            {
                return -1;
            }

            if(ExistSameNickname(ref sqlConnection, nickname))
            {
                return -2;
            }

            int count = GetCountUsers(ref sqlConnection);
            if (count < 0)
            {
                return 0;
            }

            try
            {

                SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [Users]", sqlConnection);
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    count = int.Parse(sqlReader[0].ToString());
                }
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }

                SqlCommand insertUser = new SqlCommand("INSERT [Users] (Id_User, Login, Is_Banned, Description, Nickname, Password, Items, Worn_Items, Rating, Money, Collect_Date)VALUES(@Id_User, @Login, @Is_Banned, @Description, @Nickname, @Password, @Items, @Worn_Items, @Rating, @Money, @Collect_Date)", sqlConnection);
                insertUser.Parameters.AddWithValue("Login", login);
                insertUser.Parameters.AddWithValue("Is_Banned", false);
                insertUser.Parameters.AddWithValue("Description", "");
                insertUser.Parameters.AddWithValue("Password", password);
                insertUser.Parameters.AddWithValue("Nickname", nickname);
                insertUser.Parameters.AddWithValue("Id_User", ++count);
                insertUser.Parameters.AddWithValue("Items", 14); //тут поменял ТИМЛИД
                insertUser.Parameters.AddWithValue("Worn_Items", 14); //тут поменял ТИМЛИД
                insertUser.Parameters.AddWithValue("Rating", 0);
                insertUser.Parameters.AddWithValue("Money", 0);
                insertUser.Parameters.AddWithValue("Collect_Date", DateTime.Now);
                insertUser.ExecuteNonQuery();
                return count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

        }

        protected internal int LogIn(string login, string password)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;

            if(ExistSameLogin(ref sqlConnection, login))
            {
                SqlCommand command = new SqlCommand("SELECT Id_User, Password FROM[Users] WHERE[Login] = @Login", sqlConnection);
                command.Parameters.AddWithValue("Login", login);
                try
                { 
                    sqlReader = command.ExecuteReader();
                    if (sqlReader.Read() && sqlReader["Password"].ToString().Equals(password))
                    {
                        return (int)sqlReader["Id_User"];
                    }
                    else
                    {
                        return -2;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return 0;
                }
                finally
                {
                    if (sqlReader != null)
                    {
                        sqlReader.Close();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }
                }

            }
            else
            {
                return -1;
            }
        }

        public PlayerData getPlayerById(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM[Users] WHERE[Id_User] = @id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", id);
            try
            {
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    int rating = (int)sqlReader["Rating"];
                    int money = (int)sqlReader["Money"];
                    long items = (long)sqlReader["Items"];
                    long wornItems = (long)sqlReader["Worn_Items"];
                    string login = sqlReader["Login"].ToString();
                    string nickName = sqlReader["Nickname"].ToString();
                    string description = sqlReader["Description"].ToString();
                    bool isBanned = (bool)sqlReader["Is_Banned"];
                    DateTime collectDate = (DateTime)sqlReader["Collect_Date"];
                    return new PlayerData(items, wornItems, nickName, rating, money, id, login,GetIdCastleByIdPlayer(id),description, isBanned, collectDate);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

        }

        public bool AddEventData(EventData eventData)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            int id = getIdAddedEventData(ref sqlConnection);
            if (id == -1)
            {
                return false;
            }
            else
            {
                try
                {
                    SqlCommand insertEvent = new SqlCommand("INSERT [Events] (Id_Event, Start, Finish, Rating_Award, Money_Award, Title, Description, Is_Awarded)VALUES(@Id_Event,@Start, @Finish, @Rating_Award, @Money_Award, @Title, @Description, @Is_Awarded)", sqlConnection);
                    insertEvent.Parameters.AddWithValue("Id_Event", id);
                    insertEvent.Parameters.AddWithValue("Start", eventData.start);
                    insertEvent.Parameters.AddWithValue("Finish", eventData.end);
                    insertEvent.Parameters.AddWithValue("Rating_Award", eventData.ratingAward);
                    insertEvent.Parameters.AddWithValue("Money_Award", eventData.moneyAward);
                    insertEvent.Parameters.AddWithValue("Title", eventData.title);
                    insertEvent.Parameters.AddWithValue("Description", eventData.description);
                    insertEvent.Parameters.AddWithValue("Is_Awarded", eventData.isAwarded);
                    insertEvent.ExecuteNonQuery();
                    AddManagers(id, eventData.managers, ref sqlConnection);
                    AddAdmins(id, eventData.admins, ref sqlConnection);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
                finally
                {
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }
                }

            }

        }

        private void AddManagers(int id, List<PlayerData> managers,ref SqlConnection sqlConnection)
        {
            for (int i = 0; i < managers.Count; i++)
            {
                SqlCommand insertUser = new SqlCommand("INSERT [Managers] (Id_Event, Id_User)VALUES(@Id_Event,@Id_User)", sqlConnection);
                insertUser.Parameters.AddWithValue("Id_Event", id);
                insertUser.Parameters.AddWithValue("Id_User", managers[i].Id);
                insertUser.ExecuteNonQuery();
            }
        }

        private void AddAdmins(int id, List<PlayerData> admins, ref SqlConnection sqlConnection)
        {
            for (int i = 0; i < admins.Count; i++)
            {
                SqlCommand insertUser = new SqlCommand("INSERT [Admins] (Id_Event, Id_User)VALUES(@Id_Event,@Id_User)", sqlConnection);
                insertUser.Parameters.AddWithValue("Id_Event", id);
                insertUser.Parameters.AddWithValue("Id_User", admins[i].Id);
                insertUser.ExecuteNonQuery();
            }
        }

        public EventData getEventDataById(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM[Events] WHERE[Id_Event] = @id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_Event", id);
            try
            {
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    int ratingAward = (int)sqlReader["Rating_Award"];
                    int moneyAward = (int)sqlReader["Money_Award"];
                    DateTime start = (DateTime)sqlReader["Start"];
                    DateTime end = (DateTime)sqlReader["Finish"];
                    string title = sqlReader["Title"].ToString();
                    string description = sqlReader["Description"].ToString();
                    bool isAwarded = (bool)sqlReader["Is_Awarded"];
                    List<PlayerData> admins = getAdminList(id);
                    List<PlayerData> managers = getManagerList(id);
                    return new EventData(id, start, end, title, description, ratingAward, moneyAward, admins, managers, isAwarded);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }

        }

        private List<PlayerData> getManagerList(int idEvent)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM[Managers] WHERE[Id_Event] = @id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_Event", idEvent);
            List<PlayerData> managers = new List<PlayerData>();
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int idUser = (int)sqlReader["Id_User"];
                    managers.Add(getPlayerById(idUser));
                }
                return managers;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
        }

        private List<PlayerData> getAdminList(int idEvent)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM[Admins] WHERE[Id_Event] = @id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_Event", idEvent);
            List<PlayerData> admins = new List<PlayerData>();
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int idUser = (int)sqlReader["Id_User"];
                    admins.Add(getPlayerById(idUser));
                }
                return admins;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
        }

        public int getIdAddedEventData(ref SqlConnection sqlConnection)
        {
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [Events]", sqlConnection);
            sqlReader = command.ExecuteReader();
            int count = 0;
            try
            {
                if (sqlReader.Read())
                {
                    count = int.Parse(sqlReader[0].ToString());
                    return count + 1;
                }
                else
                {
                    return -1;
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
        }

        public List<PlayerData> GetUsersByNicknameSubstring(string s)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            string commandline = "SELECT TOP(10) * FROM [Users] WHERE [Nickname] LIKE '%" + s + "%'";
            SqlCommand command = new SqlCommand(commandline, sqlConnection);
            try
            {
                List<PlayerData> players = new List<PlayerData>();
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    string description = sqlReader["Description"].ToString();
                    bool isBanned = bool.Parse(sqlReader["Is_Banned"].ToString());
                    int id = (int)sqlReader["Id_User"];
                    int rating = (int)sqlReader["Rating"];
                    int money = (int)sqlReader["Money"];
                    long items = (long)sqlReader["Items"];
                    long wornItems = (long)sqlReader["Worn_Items"];
                    string login = sqlReader["Login"].ToString();
                    string nickName = sqlReader["Nickname"].ToString();
                    DateTime collectDate = (DateTime)sqlReader["Collect_Date"];
                    players.Add(new PlayerData(items, wornItems, nickName, rating, money, id, login, GetIdCastleByIdPlayer(id), description, isBanned, collectDate));
                }
                return players;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PlayerData>();
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<ReviewData> GetReviews(int idEvent)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT Users.Id_User, Nickname, Review, [DataReview] FROM [xrefUsersEvents] INNER JOIN [Users] ON xrefUsersEvents.Id_User = Users.Id_User AND id_Event = @Id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_Event", idEvent);
            try
            {

                List<ReviewData> reviews = new List<ReviewData>();
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int id = (int)sqlReader["Id_User"];
                    string nickname = (string)sqlReader["Nickname"];
                    string review = sqlReader["Review"].ToString();
                    DateTime reviewData = (DateTime)sqlReader["DataReview"];
                    reviews.Add(new ReviewData(id, nickname, review, reviewData));
                }
                return reviews;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ReviewData>();
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<EventUserData> GetFutureEvents(int userId)
        {
            DateTime dt = DateTime.Now;
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            string commandline = "SELECT * FROM [Events] FULL JOIN [xrefUsersEvents] ON xrefUsersEvents.Id_Event = Events.Id_Event AND xrefUsersEvents.Id_User = @Id_User AND Events.Start >= CAST('" + dt.ToString() + "' as DATE)";
            SqlCommand command = new SqlCommand(commandline, sqlConnection);
            command.Parameters.AddWithValue("DataNow", dt.ToString());
            command.Parameters.AddWithValue("Id_User", userId);
            try
            {
                List<EventUserData> events = new List<EventUserData>();
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int id = (int)sqlReader["Id_Event"];
                    int ratingAward = (int)sqlReader["Rating_Award"];
                    int moneyAward = (int)sqlReader["Money_Award"];
                    DateTime start = (DateTime)sqlReader["Start"];
                    DateTime end = (DateTime)sqlReader["Finish"];
                    string title = sqlReader["Title"].ToString();
                    string description = sqlReader["Description"].ToString();
                    
                    bool registred = !(sqlReader["Id_User"] == null);
                    List<PlayerData> admins = getAdminList(id);
                    List<PlayerData> managers = getManagerList(id);
                    events.Add(new EventUserData(id, start, end, title, description, ratingAward, moneyAward, admins, managers, registred));
                }
                return events;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<EventUserData>();
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }


        public List<EventData> GetMyRegisteredEvents(int idUser)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT TOP(30) Events.Id_Event, Start, Finish, Rating_Award, Money_Award, Title, Description, Is_Awarded FROM Events INNER JOIN xrefUsersEvents ON Events.Id_Event = xrefUsersEvents.Id_Event AND xrefUsersEvents.Id_User = @Id_User ORDER BY Start DESC", sqlConnection);
            command.Parameters.AddWithValue("Id_User", idUser);
            command.Parameters.AddWithValue("TimeNow", DateTime.Now);
            try
            {
                List<EventData> events = new List<EventData>();
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int id = (int)sqlReader["Id_Event"];
                    int ratingAward = (int)sqlReader["Rating_Award"];
                    int moneyAward = (int)sqlReader["Money_Award"];
                    DateTime start = (DateTime)sqlReader["Start"];
                    DateTime end = (DateTime)sqlReader["Finish"];
                    string title = sqlReader["Title"].ToString();
                    string description = sqlReader["Description"].ToString();
                    List<PlayerData> admins = getAdminList(id);
                    List<PlayerData> managers = getManagerList(id);
                    bool isAwarded = (bool)sqlReader["Is_Awarded"];
                    events.Add(new EventData(id, start, end, title, description, ratingAward, moneyAward, admins, managers, isAwarded));
                }
                return events;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<EventData>();
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void DeleteMyEvent(int idUser, int idEvent)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("DELETE FROM [xrefUsersEvents] WHERE [Id_User] = @Id_User AND [Id_Event] = @Id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_User", idUser);
            command.Parameters.AddWithValue("Id_Event", idEvent);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AddMyEvent(int idUser, int idEvent)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("INSERT INTO [xrefUsersEvents] (Id_User, Id_Event) VALUES (@Id_User, @Id_Event)", sqlConnection);
            command.Parameters.AddWithValue("Id_User", idUser);
            command.Parameters.AddWithValue("Id_Event", idEvent);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AddReview(int idUser, int idEvent, string review, DateTime dateReview)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [xrefUsersEvents]  SET xrefUsersEvents.Review = @Review, xrefUsersEvents.DateReview = @DateReview WHERE Id_User = @Id_User AND Id_Event = @Id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_User", idUser);
            command.Parameters.AddWithValue("Id_Event", idEvent);
            command.Parameters.AddWithValue("Review", review);
            command.Parameters.AddWithValue("DateReview", dateReview);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<PlayerData> GetPlayersRaiting()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM [Users] ORDER BY Rating DESC", sqlConnection);
            List<PlayerData> players = new List<PlayerData>();
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    
                    int id = (int)sqlReader["Id_User"];
                    int rating = (int)sqlReader["Rating"];
                    int money = (int)sqlReader["Money"];
                    long items = (long)sqlReader["Items"];
                    long wornItems = (long)sqlReader["Worn_Items"];
                    string login = sqlReader["Login"].ToString();
                    string nickName = sqlReader["Nickname"].ToString();
                    string description = sqlReader["Description"].ToString();
                    bool isBanned = bool.Parse(sqlReader["Is_Banned"].ToString());
                    DateTime collectDate = (DateTime)sqlReader["Collect_Date"];
                    players.Add(new PlayerData(items, wornItems, nickName, rating, money, id, login, GetIdCastleByIdPlayer(id), description, isBanned, collectDate));
                }
                return players;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PlayerData>();
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public int GetIdCastleByIdPlayer(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT Id_Castle FROM [xrefCastleUser] WHERE id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", id);
            try
            {
                sqlReader = command.ExecuteReader();
                int idCastle = -1;
                while (sqlReader.Read())
                {
                    idCastle = (int)sqlReader["Id_Castle"];
                }
                return id;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void UpdateDescriptionUser(int id, string description)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [Users]  SET Users.Description = @Description WHERE Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", id);
            command.Parameters.AddWithValue("Description", description);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void UpdateIsBannedUser(int id, bool isBanned)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [Users]  SET Users.Is_Banned = @Is_Banned WHERE Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", id);
            command.Parameters.AddWithValue("Is_Banned",isBanned);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AwardPlayer(int id, int rating, int money)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [Users]  SET Users.Rating = @Rating, User.Money = @Money WHERE Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Rating", rating);
            command.Parameters.AddWithValue("Money", money);
            command.Parameters.AddWithValue("Id_User", id);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AwardEvent(int id)
        {

            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT Users.Id_User, Users.Money + Events.Money_Award AS Money_User, Users.Rating + Events.Rating_Award AS User_Rating FROM xrefUsersEvents INNER JOIN  Events ON Events.Id_Event = xrefUsersEvents.Id_Event AND Events.Is_Awarded = 0 AND Events.Id_Event = @Id_Event INNER JOIN  Users ON Users.Id_User = xrefUsersEvents.Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_Event", id);
            try
            {
                List<EventData> events = new List<EventData>();
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int idUser = (int)sqlReader["Id_User"];
                    int rating = (int)sqlReader["User_Rating"];
                    int money = (int)sqlReader["Money_User"];
                    AwardPlayer(idUser, rating, money);
                    UpdateEventIsAwarded(id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }


        public List<EventData> GetNotAwardedEvents()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM [Events] WHERE Is_Awarded = @Is_Awarded", sqlConnection);
            command.Parameters.AddWithValue("Is_Awarded", false);
            try
            {
                List<EventData> events = new List<EventData>();
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int id = (int)sqlReader["Id_Event"];
                    int ratingAward = (int)sqlReader["Rating_Award"];
                    int moneyAward = (int)sqlReader["Money_Award"];
                    DateTime start = (DateTime)sqlReader["Start"];
                    DateTime end = (DateTime)sqlReader["Finish"];
                    string title = sqlReader["Title"].ToString();
                    string description = sqlReader["Description"].ToString();
                    List<PlayerData> admins = getAdminList(id);
                    List<PlayerData> managers = getManagerList(id);
                    bool isAwarded = (bool)sqlReader["Is_Awarded"];
                    events.Add(new EventData(id, start, end, title, description, ratingAward, moneyAward, admins, managers, isAwarded));
                }
                return events;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<EventData>();
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void UpdateEventIsAwarded(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            try
            {
                SqlCommand command = new SqlCommand("UPDATE [Events]  SET Events.Is_Awarded = @Is_Awarded WHERE Id_Event = @Id_Event", sqlConnection);
                command.Parameters.AddWithValue("Id_Event", id);
                command.Parameters.AddWithValue("Is_Awarded", true);
                command.ExecuteNonQuery();
            }    
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public bool IsOnEvent(int IdUser, int IdEvent)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [xrefUsersEvents] WHERE Id_User = @Id_User AND Id_Event = @Id_Event", sqlConnection);
            command.Parameters.AddWithValue("Id_Event", IdEvent);
            command.Parameters.AddWithValue("Id_User", IdUser);
            int count = 0;
            try
            {
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    if (int.TryParse(sqlReader[0].ToString(), out count) && count != 0)
                    {
                        return true;
                    }
                    return false;
                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

    }
}

