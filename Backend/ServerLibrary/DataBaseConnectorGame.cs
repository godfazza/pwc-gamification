﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary;

namespace ServerLibrary
{
    public class DataBaseConnectorGame
    {
        const string connectionString = @"Data Source=PwCGamification;Initial Catalog=PwC_Gamification;Integrated Security=True";

        public CastleInfo GetCastle(int Id)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM [Castles] WHERE Id_Castle = @Id_Castle", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", Id);
            try
            {
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    int castleID = Id;
                    string castleName = sqlReader["Name"].ToString();
                    int castleLevel = GetCastlelevel(Id);
                    PlayerData leader = null;
                    List<PlayerData> coleaders= new List<PlayerData>();
                    List<PlayerData> clanMembers = GetClanMembers(Id,ref leader, ref coleaders);
                    List<BuildingInfo> buildings = GetBuildings(Id);
                    int eventMoney = (int)sqlReader["Event_Money"];
                    int marketMoney = (int)sqlReader["Market_Money"];
                    DateTime attacDate = (DateTime)sqlReader["Attac_Date"];
                    return new CastleInfo(castleID, castleName, castleLevel, clanMembers, buildings, coleaders, leader, eventMoney, marketMoney, attacDate);
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public int GetCastlelevel(int Id)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            int level = 0;
            SqlCommand command = new SqlCommand("SELECT AVG(Level) AS Level FROM xrefCastleBuilding WHERE Id_Castle = @Id_Castle", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", Id);
            try
            {
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    level = (int)sqlReader["Level"];
                }
                return level;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 1;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<PlayerData> GetClanMembers(int Id, ref PlayerData leader, ref List<PlayerData> coleaders)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            List<PlayerData> members = new List<PlayerData>();
            SqlCommand command = new SqlCommand("SELECT Users.Id_User, Login, Nickname, Items, Worn_Items, Rating, Money, Description, Is_Banned, Is_Leader, Is_Coleader, Collect_Date FROM xrefCastleUser INNER JOIN Users ON xrefCastleUser.Id_User = Users.Id_User AND xrefCastleUser.Id_Castle = @Id_Castle", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", Id);
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    int idUser = (int)sqlReader["Id_User"];
                    int rating = (int)sqlReader["Rating"];
                    int money = (int)sqlReader["Money"];
                    long items = (long)sqlReader["Items"];
                    long wornItems = (long)sqlReader["Worn_Items"];
                    string login = sqlReader["Login"].ToString();
                    string nickName = sqlReader["Nickname"].ToString();
                    string description = sqlReader["Description"].ToString();
                    bool isBanned = (bool)sqlReader["Is_Banned"];
                    bool isLeader = (bool)sqlReader["Is_Leader"];
                    bool isColeader = (bool)sqlReader["Is_Coleader"];
                    DateTime collectDate = (DateTime)sqlReader["Collect_Date"];
                    members.Add(new PlayerData(items, wornItems, nickName, rating, money, idUser, login, Id, description, isBanned,collectDate));
                    if (isLeader)
                    {
                        leader = new PlayerData(items, wornItems, nickName, rating, money, idUser, login, Id, description, isBanned, collectDate);
                    }
                    if (isColeader)
                    {
                        coleaders.Add(new PlayerData(items, wornItems, nickName, rating, money, idUser, login, Id, description, isBanned, collectDate));
                    }
                }
                return members;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return members;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<BuildingInfo> GetBuildings(int idCastle)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            List<BuildingInfo> buildings = new List<BuildingInfo>();
            SqlCommand command = new SqlCommand("SELECT * FROM xrefCastleBuilding INNER JOIN Buildings ON xrefCastleBuilding.Id_Building = Buildings.Id_Building AND Id_Castle = @Id_Castle", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", idCastle);
            try
            {
                sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    string name = sqlReader["Name"].ToString();
                    int number = (int)sqlReader["Id_Building"];
                    int level = (int)sqlReader["Level"];
                    int introducedResources = (int)sqlReader["Introduced_Resources"];
                    buildings.Add(new BuildingInfo(name, number, level, introducedResources));
                }
                return buildings;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return buildings;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void EnterToCastle(int IdUser, int IdCastle)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand insertEvent = new SqlCommand("INSERT INTO  [xrefCastleUser] (Id_Castle, Id_User, Is_Leader, Is_Coleader)VALUES(@Id_Castle, @Id_User, @Is_Leader, @Is_Coleader)", sqlConnection);
            insertEvent.Parameters.AddWithValue("Id_Castle", IdCastle);
            insertEvent.Parameters.AddWithValue("Id_User", IdUser);
            insertEvent.Parameters.AddWithValue("Is_Leader", false);
            insertEvent.Parameters.AddWithValue("Is_Coleader", false);
            try
            {
                insertEvent.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void LeaveCastle(int IdUser, int IdCastle)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand delete = new SqlCommand("DELETE xrefCastleUser WHERE Id_Castle = @Id_Castle AND Id_User = @Id_User", sqlConnection);
            delete.Parameters.AddWithValue("Id_Castle", IdCastle);
            delete.Parameters.AddWithValue("Id_User", IdUser);
            try
            {

                delete.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void SetLeader(int IdUser, int IdCastle)
        {
            RemoveLeader(IdCastle);
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [xrefCastleUser]  SET xrefCastleUser.Is_Leader = @Is_Leader WHERE Id_Castle = @Id_Castle AND Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            command.Parameters.AddWithValue("Id_User", IdUser);
            command.Parameters.AddWithValue("Is_Leader", true);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void RemoveLeader(int IdCastle)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [xrefCastleUser]  SET xrefCastleUser.Is_Leader = @Is_Leader, xrefCastleUser.Is_Coleader = @Is_Coleader WHERE Id_Castle = @Id_Castle AND Is_Leader = @Leader", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            command.Parameters.AddWithValue("Is_Leader", false);
            command.Parameters.AddWithValue("Leader", true);
            command.Parameters.AddWithValue("Is_Coleader", true);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public bool IsLeader(int IdCastle, int IdUser)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            bool res = false;
            List<BuildingInfo> buildings = new List<BuildingInfo>();
            SqlCommand command = new SqlCommand("SELECT xrefCastleUser.Is_Leader FROM xrefCastleUser WHERE Id_Castle = @Id_Castle AND Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", IdUser);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            try
            {
                if (sqlReader.Read())
                {
                    res = (bool)sqlReader["Is_Leader"];;
                }
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return res;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }



        public void SetColeaderPosition(int IdUser,int IdCastle, bool IsColeader)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [xrefCastleUser]  SET xrefCastleUser.Is_Coleader = @Is_Coleader WHERE Id_Castle = @Id_Castle AND Id_User = @Id_User AND Is_Leader = @Is_Leader", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            command.Parameters.AddWithValue("Id_User", IdUser);
            command.Parameters.AddWithValue("Is_Coleader", IsColeader);
            command.Parameters.AddWithValue("Is_Leader", false);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public bool IsColeader(int IdCastle, int IdUser)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            bool res = false;
            List<BuildingInfo> buildings = new List<BuildingInfo>();
            SqlCommand command = new SqlCommand("SELECT xrefCastleUser.Is_Coleader FROM xrefCastleUser WHERE Id_Castle = @Id_Castle AND Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", IdUser);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            try
            {
                if (sqlReader.Read())
                {
                    res = (bool)sqlReader["Is_Coleader"]; ;
                }
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return res;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public bool IsLeadPosition(int IdCastle, int IdUser)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            bool res = false;
            List<BuildingInfo> buildings = new List<BuildingInfo>();
            SqlCommand command = new SqlCommand("SELECT xrefCastleUser.Is_Coleader, xrefCastleUser.Is_Leader FROM xrefCastleUser WHERE Id_Castle = @Id_Castle AND Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", IdUser);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            try
            {
                if (sqlReader.Read())
                {
                    res = (bool)sqlReader["Is_Coleader"] || (bool)sqlReader["Is_Leader"];
                }
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return res;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AddGameEvent(ReviewData rd)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
        SqlCommand insertEvent = new SqlCommand("INSERT INTO GameEvent (Id_Game_Event, Id_Castle, Details, Date_Event) VALUES (@Id_Game_Event, @Id_Castle, @Details, @Date_Event)", sqlConnection);
            insertEvent.Parameters.AddWithValue("Id_Game_Event", GetIdNewGameEvent());
            insertEvent.Parameters.AddWithValue("Id_Castle", rd.id);
            insertEvent.Parameters.AddWithValue("Details", rd.review);
            insertEvent.Parameters.AddWithValue("Date_Event", rd.date);
            try
            {
                insertEvent.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public int GetIdNewGameEvent()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            int id = 0;
            SqlCommand command = new SqlCommand("SELECT COUNT(*) AS Count FROM GameEvent", sqlConnection);
            try
            {
                sqlReader = command.ExecuteReader();
                if (sqlReader.Read())
                {
                    id = (int)sqlReader["Count"];
                }
                return id+1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<ReviewData> GetGameEvents(int idCastle)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlDataReader sqlReader = null;
            List<ReviewData> gameEvents = new List<ReviewData>();
            SqlCommand command = new SqlCommand("SELECT TOP(100)* FROM GameEvent WHERE Id_Castle = @Id_Castle ORDER BY Date_Event DESC", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", idCastle);
            try
            {
                sqlReader = command.ExecuteReader();
                while(sqlReader.Read())
                {
                    int id = idCastle;
                    string nickname = "";
                    string review = sqlReader["Details"].ToString();
                    DateTime date = (DateTime)sqlReader["Date_Event"];
                    gameEvents.Add(new ReviewData(id, nickname, review, date));
                }
                return gameEvents;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return gameEvents;
            }
            finally
            {
                if (sqlReader != null)
                {

                    sqlReader.Close();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }


        public void UpdateCastelAttac(int Id, DateTime attacTime)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [Castles]  SET Castles.Attac_Date = @Attac_Date WHERE Id_Castle = @Id_Castle", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", Id);
            command.Parameters.AddWithValue("Attac_Date", attacTime);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void UpdateUser(PlayerData pd)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [Users]  SET Users.Nickname = @Nickname, Users.Items = @Items,  Users.Worn_Items = @Worn_Items,  Users.Money = @Money,  Users.Description = @Description,  Users.Is_Banned = @Is_Banned, Collect_Date = @Collect_Date WHERE Id_User = @Id_User", sqlConnection);
            command.Parameters.AddWithValue("Id_User", pd.Id);
            command.Parameters.AddWithValue("Nickname", pd.NickName);
            command.Parameters.AddWithValue("Items", pd.Items);
            command.Parameters.AddWithValue("Worn_Items", pd.WornItems);
            command.Parameters.AddWithValue("Money", pd.Money);
            command.Parameters.AddWithValue("Description", pd.Description);
            command.Parameters.AddWithValue("Is_Banned", pd.isBanned);
            command.Parameters.AddWithValue("Collect_Date", pd.collectDate);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }


        public void UpdateBuilding(int IdCastle, BuildingInfo bi)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [xrefCastleBuilding]  SET xrefCastleBuilding.Introduced_Resources = @Introduced_Resources, xrefCastleBuilding.Level = @Level WHERE Id_Castle = @Id_Castle AND Id_Building = @Id_Building", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            command.Parameters.AddWithValue("Id_Building", bi.number);
            command.Parameters.AddWithValue("Introduced_Resources", bi.introducedResources);
            command.Parameters.AddWithValue("Level", bi.level);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }


        public void AddMoneyToCastle(int IdCastle, int addEventMoney, int addMarketMoney)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("UPDATE [Castles]  SET Castles.Event_Money = Castles.Event_Money + @Event_Money, Castles.Market_Money = Castles.Market_Money + @Market_Money WHERE Id_Castle = @Id_Castle", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", IdCastle);
            command.Parameters.AddWithValue("Event_Money", addEventMoney);
            command.Parameters.AddWithValue("Market_Money", addMarketMoney);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

    }
}
