﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ServerAlgo;

namespace ServerLibrary
{
    public class ServerUpdater
    {
        private DataBaseConnector dataBaseConnector;
        private GenerateAlgorithm generate;
        private DataBaseUpdate dataBaseUpdate;
        private DateTime lastMapUpdate = new DateTime();
        List<ClientObject> onlinePlayers;
        public ServerUpdater(DataBaseConnector dbc, List<ClientObject> onlinePlayers, DataBaseUpdate dbu)
        {
            dataBaseConnector = dbc;
            this.dataBaseUpdate = dbu;
            this.onlinePlayers = onlinePlayers;
            lastMapUpdate = DateTime.Now;
            GenerateMap();
        }
        public void GenerateMap()
        {
            generate = new GenerateAlgorithm( 64, 64, DateTime.Now );
            lastMapUpdate = DateTime.Now;
            var map = generate.GenerateMap();
            generate.GenerateCastles( map );
            var castles = generate.GetCastles();
            dataBaseUpdate.ClearAll();
            dataBaseUpdate.AddEmptyBuildings(BuildingManager.InitializeBuilding());
            foreach ( var key in castles.Keys )
            {
                dataBaseUpdate.AddEmptyCastle(key, castles[key]);
            }
        }
        public void Process()
        {
            while ( true )
            {
                try
                {
                    if ( DateTime.Now.Month != lastMapUpdate.Month )
                    {
                        GenerateMap();
                    }
                    else
                    {
                        List<EventData> lastNotAwardedEvents = new List<EventData>();
                        lastNotAwardedEvents = dataBaseConnector.GetNotAwardedEvents();
                        if ( lastNotAwardedEvents != null && lastNotAwardedEvents.Count > 0 )
                        {
                            foreach ( var item in lastNotAwardedEvents )
                            {
                                EventAward( item );
                            }
                        }

                        Thread.Sleep( 1000 );
                    }
                }
                catch (Exception exc )
                {
                    Console.WriteLine("ServUpdater line 41 "+ exc.Message);
                }
            }
        }
        private void EventAward(EventData e)
        {
            if ( e == null || e.isAwarded == true)
                return;
            dataBaseConnector.AwardEvent( e.id );
            if (onlinePlayers != null && onlinePlayers.Count > 0)
            {
                foreach ( var client in onlinePlayers )
                {
                    if (client != null && client.player != null)
                    {
                        //для каждого пользователя проверить принадлежит ли он к данному событию если да, то отправить данные
                        if ( dataBaseConnector.IsOnEvent( client.player.Id, e.id ) )
                        {
                            client.player = dataBaseConnector.getPlayerById( client.player.Id );
                            client.SendMessage(new ResponseUpdate(new ResponseUser( ResponseUser.ResponseUserType.EventUpdate,client.player),
                                ResponseUpdate.ResponseUpdateType.UpdateEventReviews));
                        }
                    }
                }
            }
            
        }
        
    }
}
