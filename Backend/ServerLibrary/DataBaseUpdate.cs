﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary
{
    public class DataBaseUpdate
    {
        const string connectionString = @"Data Source=PwCGamification;Initial Catalog=PwC_Gamification;Integrated Security=True";



        public void AddEmptyCastle(int castleID, string castleName)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("INSERT INTO [Castles] (Id_Castle, Name, Event_Money, Market_Money, Attac_Date) VALUES (@Id_Castle, @Name, @Event_Money, @Market_Money, @Attac_Date)", sqlConnection);
            command.Parameters.AddWithValue("Id_Castle", castleID);
            command.Parameters.AddWithValue("Name", castleName);
            command.Parameters.AddWithValue("Event_Money", 0);
            command.Parameters.AddWithValue("Market_money", 0);
            command.Parameters.AddWithValue("Attac_Date", DateTime.Now);
            try
            {
                command.ExecuteNonQuery();
                AddBuildigsToCastle(castleID);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AddEmptyBuildings(List<BuildingInfo> buildings)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            try
            {
                for (int i = 0; i < buildings.Count; i++)
                {
                    SqlCommand command = new SqlCommand("INSERT INTO [Buildings] (Id_Building, Name) VALUES (@Id_Building, @Name)", sqlConnection);
                    command.Parameters.AddWithValue("Id_Building", buildings[i].number);
                    command.Parameters.AddWithValue("Name", buildings[i].name);
                    command.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void AddBuildigsToCastle(int idCastle)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    SqlCommand command = new SqlCommand("INSERT INTO [xrefCastleBuilding] (Id_Castle, Id_Building, Introduced_Resources, Level) VALUES (@Id_Castle, @Id_Building, @Introduced_Resources, @Level)", sqlConnection);
                    command.Parameters.AddWithValue("Id_Castle", idCastle);
                    command.Parameters.AddWithValue("Id_Building", i);
                    command.Parameters.AddWithValue("Introduced_Resources", 0);
                    command.Parameters.AddWithValue("Level", 1);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void ClearAll()
        {
            ClearCastleUser();
            ClearCastleBuilding();
            ClearGameEvents();
            ClearBuildings();
            ClearCastle();
        }

        public void ClearCastle()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("DELETE FROM Castles", sqlConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void ClearCastleUser()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("TRUNCATE TABLE xrefCastleUser", sqlConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void ClearCastleBuilding()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("TRUNCATE TABLE xrefCastleBuilding", sqlConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void ClearBuildings()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("DELETE FROM Buildings", sqlConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void ClearGameEvents()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand("TRUNCATE TABLE GameEvent", sqlConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}
