﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerAlgo;






namespace Some_Testing
{
    
    using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

 
    /// <summary>
    /// Класс определяющий все доступные характеристики игры
    /// </summary>
    [Serializable]
    public class Power
    {
        public Power() : this(0, 0, 0)
        {
        }

        public Power(int attack, int health, int price)
        {
            this.Attack = attack;
            this.Health = health;
            this.Price = price;
        }

        public int Attack;
        public int Health;
        public int Price;

        public void AddPower(Power powerToAdd)
        {
            this.Attack += powerToAdd.Attack;
            this.Health += powerToAdd.Health;
            this.Price += powerToAdd.Price;
        }

        public static Power operator +(Power a, Power b)
        {
            return new Power(a.Attack + b.Attack, a.Health + b.Health, a.Price + b.Price);
        }
    }

    [Serializable]
    public class ItemList
    {
        /// <summary>
        /// Список всех предметов игры
        /// </summary>
        public static List<Item> Items = new List<Item>
        {
            //office
            new Item(1, 1, 0, 0, ItemType.Body),
            new Item(2, 1, 0, 0, ItemType.Head),
            new Item(3, 0, 1, 0, ItemType.Weapon),

            //warrior
            new Item(4, 4, 0, 3, ItemType.Body),
            new Item(5, 2, 3, 3, ItemType.Head),
            new Item(6, 0, 3, 3, ItemType.Weapon),

            //ninja
            new Item(7, 8, 3, 8, ItemType.Body),
            new Item(9, 0, 5, 8, ItemType.Weapon),

            //king
            new Item(10, 10, 1, 16, ItemType.Body),
            new Item(11, 8, 4, 16, ItemType.Head),
            new Item(12, 8, 3, 16, ItemType.Weapon),

            //princess
            new Item(13, 3, 1, 3, ItemType.Body),
            new Item(14, 2, 2, 2, ItemType.Head),
            new Item(15, 5, 15, 20, ItemType.Weapon)
        };


        /// <summary>
        /// Выделяет из кода все предметы, записанные в нем и записывает их ID  список
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static List<long> GetIds(long code)
        {
            List<long> longList = new List<long>();
            long num = 1;
            for (int index = 0; index <= 63; ++index)
            {
                if (code / (long) num % 2L == 1L)
                    longList.Add((long) index);
                num *= 2;
            }
            return longList;
        }


        /// <summary>
        /// Возвращает обьект класса Power, в котором содержатся все характеристики
        /// </summary>
        /// <param name="code">ID надетых предметов</param>
        public static Power GetPower(long code)
        {
            Power ans = new Power();
            List<long> ids = GetIds(code);
            for (int i = 0; i < ids.Count; i++)
            {
                for (int j = 0; j < Items.Count; j++)
                {
                    if (ids[i] == Items[j].Id)
                    {
                        ans.AddPower(Items[j].ItemPower);
                    }
                }
            }

            return ans;
        }
    } 
    
    public enum ItemType
    {
        Head,
        Weapon,
        Body
    }
    [Serializable]
    public class Item
    {
        public Power ItemPower;
        public int Id; 
        public ItemType Type;

        public Item(int id, int health, int attack, int price, ItemType type)
        {
            Id = id; 
            Type = type;
            ItemPower = new Power {Attack = attack, Health = health, Price = price};
        }
    }
    
    class Program
    {
       public static void Main(string[] args)
       {
           List<long> ans = ItemList.GetIds(14);
       }
        
    }
}
