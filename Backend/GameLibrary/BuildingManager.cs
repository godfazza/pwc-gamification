using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class BuildingManager
    {

        private static int collectDelta = 12;

        public static int NextLevelPrice(BuildingInfo building)
        {
            return 5 * (int)Math.Pow(2, building.level - 1);
        }
        public static int MoneyToCollect(System.DateTime lastCollect, BuildingInfo market)
        {
            if((lastCollect - System.DateTime.Now).Hours >= collectDelta )
            {
                return market.level;
            }
            else
            {
                return 0;
            }
        }
        public static List<BuildingInfo> InitializeBuilding()
        {
            List<BuildingInfo> buildings = new List<BuildingInfo>();
            buildings.Add(new BuildingInfo("The government square", 0, 1, 0));
            buildings.Add(new BuildingInfo("Farm", 1, 1 ,0));
            buildings.Add(new BuildingInfo("Market", 2, 1, 0));
            buildings.Add(new BuildingInfo("War camp", 3, 1 ,0));
            return buildings;
        }
        public static bool IsEventMoneyBuilding(BuildingInfo building)
        {
            if (building.number == 2)
            {
                return true;
            }
            return false;
        }
        public static BuildingInfo GetNewBuilding(BuildingInfo building)
        {
            building.introducedResources++;
            if (building.introducedResources >= 5 * (int)Math.Pow(2, building.level - 1))
            {
                building.introducedResources -= 5 * (int)Math.Pow(2, building.level - 1);
                building.level++;
            }
            return building;
        }
    }
}