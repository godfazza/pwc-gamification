﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ResponseUser : ResponseInfo
    {
        public ResponseUser(ResponseUserType type, PlayerData data) : base(ResponseType.ResponseUserInfo)
        {
            responseUserType = type;
            this.data = data;
        }
        public readonly PlayerData data;
        public readonly ResponseUserType responseUserType;
        public enum ResponseUserType
        {
            Authorization,
            GetById,
            PurchaseOk,
            PurchaseReject,
            EventUpdate,
            DescriptionUpdate
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
