﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class GetByIdRequest:RequestInfo
    {
        public readonly int id;
        public readonly GetById getByIdType;
        public GetByIdRequest(int id, GetById type) : base(RequestType.GetById)
        {
            this.id = id;
            getByIdType = type;
        }
        public enum GetById
        {
            User,
            Event,
            RegisteredEvents,
            Reviews,
            Castle,
            CastleReviews,
            CollectMoney, //Собрать монеты int userId
            RequestString
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
