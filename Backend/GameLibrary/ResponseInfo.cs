﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ResponseInfo : DataInfo
    {
        public readonly ResponseType responseType;
        public enum ResponseType
        {
            RegistrationOk = 1,
            AutorizationOk = 2,
            AutorizationFailed_WrongPassword = 3,
            AutorizationFalied_WrongLogin = 4,
            RegistartionFailed_ExistLogin = 5,
            Error = 6,
            RegistrationFailed_ExistNickname = 7,
            ResponseUserInfo = 8,
            ResponseReviews = 9,
            ResponseEvents = 10,
            ResponseUsers = 11,
            ResponseUserEvents = 12,
            ResponseCastle,
            ResponseMap,
            ResponseMainInfo,
            ResponseUpdate,
            OkLeaveCastle,
            OkEnterCastle,
            Reload
           
        }
        public ResponseInfo(ResponseType type) : base(DataType.ResponseInfo)
        {
            responseType = type;
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
