using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class RequestInfo : DataInfo
    {
        public RequestInfo(RequestType type) : base(DataType.RequestInfo)
        {
            this.requestType = type;
        }
        public readonly RequestType requestType;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
        public enum RequestType
        {
            Registration,
            Autorization,
            GetById,
            ChangeSubscription,
            PeopleByName,
            AddEvent,
            FutureEvents,
            Map,
            FirstPlayers,
            MainInfo,
            CreateArmy

        }
        public override string ToString()
        {
            return type.ToString(); 
        }
    }
}
