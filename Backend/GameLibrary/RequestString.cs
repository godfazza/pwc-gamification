﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class RequestString : GetByIdRequest
    {
        public RequestString(int id, string str, RequestStringType requestStringType) : base( id, GetById.RequestString )
        {
            this.String = str;
            this.requestStringType = requestStringType;
        }
        public readonly string String;
        public readonly RequestStringType requestStringType;
        public enum RequestStringType
        {
            ChangeUserDescription,
            ChangeCastleName,
            AddEventReview
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
    }
}

