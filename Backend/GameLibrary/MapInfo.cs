﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class MapInfo : DataInfo
    {
        public MapInfo(string map, List<int> castlesId, DateTime creationTime):base(DataType.MapInfo)
        {
            this.map = map;
            this.creationTime = creationTime;
        }
        public string map;
        public DateTime creationTime;
        public List<int> castlesId;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
        public string GenerateMap()
        {
            return null;
        }
        public List<int> GetCastelsId()
        {
            return null;
        }
    }
}
