using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public abstract class DataInfo
    {
        public DataInfo(DataType type)
        {
            this.type = type;
        }
        public enum DataType
        {
            PlayerInfo = 1,
            MapInfo = 2,
            RequestInfo = 3,
            ResponseInfo = 4,
            EventInfo = 5,
            ReviewData = 6,
            CastleInfo = 7,
            BuildingInfo = 8
        }
        public readonly DataType type;
        public override string ToString()
        {
            return type.ToString();
        }
        public abstract byte[] Serialize();
        public static DataInfo Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            stream.Write(data, 0, data.Length);
            stream.Seek(0, SeekOrigin.Begin);
            return (DataInfo)formatter.Deserialize(stream);
        }
    }
}
