﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class EventUserData : EventData
    {
        public EventUserData(int id, DateTime start, DateTime end, string title, string description, int ratingAward, int moneyAward, List<PlayerData> admins, List<PlayerData> managers, bool v) : base(id, start, end, title, description, ratingAward, moneyAward, admins, managers)
        {
            this.visited = v;
        }
        public readonly bool visited;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
