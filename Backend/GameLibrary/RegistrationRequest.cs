﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class RegistrationRequest:RequestInfo
    {
        public string Password;
        public string EMail;
        public string NickName;
        public RegistrationRequest(string NickName, string EMail, string Password):base(RequestType.Registration)
        {
            this.EMail = EMail;
            this.Password = Password;
            this.NickName = NickName;
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
