﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ReviewData : DataInfo
    {
        public ReviewData(int id, string nickname, string review, DateTime reviewDate) : base(DataType.ReviewData)
        {
            this.id = id;
            this.nickname = nickname;
            this.review = review;           
            this.date = reviewDate;
        }
        public readonly int id;
        public readonly string nickname;
        public readonly string review;
        public readonly DateTime date;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
