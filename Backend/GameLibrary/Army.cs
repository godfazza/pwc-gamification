﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class Army
    {
        public Army(List<PlayerData> soldiers, PlayerData leader, int targetCastleId)
        {
            this.soldiers = soldiers;
            armyPower = new Power();
            for ( int i = 0 ; i < soldiers.Count ; i++ )
            {
                armyPower += ItemList.GetPower( soldiers[i].Items );
            }
            this.leader = leader;
            this.targetCastleId = targetCastleId;
        }
        public readonly List<PlayerData> soldiers;
        public readonly PlayerData leader;
        public readonly Power armyPower;
        public readonly int targetCastleId; 
    }
}
