﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ResponseCastle : ResponseInfo
    {
        public ResponseCastle(CastleInfo castle) : base( ResponseType.ResponseCastle )
        {
            this.castle = castle;
        }
        public readonly CastleInfo castle;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
    }
}
