﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class RequestAddEvent : RequestInfo
    {
        public readonly EventData eventData;
        public RequestAddEvent(EventData eventData) : base(RequestType.AddEvent)
        {
            this.eventData = eventData;
        }

        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
