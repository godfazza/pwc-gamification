﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class RequestChangeSubscription : RequestInfo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"> ID игрока</param>
        /// <param name="targetId"> ID замка или события</param>
        /// <param name="subscriptionType"> Выбрать из списка</param>
        public RequestChangeSubscription(int userId, int targetId, SubscriptionType subscriptionType) : base(RequestType.ChangeSubscription)
        {
            this.userId = userId;
            this.targetId = targetId;
            this.subscriptionType = subscriptionType;
        }

        public enum SubscriptionType{
            addUserToEvent,
            deleteUserFromEvent,
            joinToCastle,
            exitFromCastle,
            changeLeader,
            makeAColeader,
            unmakeAColeader,
            buyItem,
            updateBuilding, // int buildingID, int donateToBuilding
            donateToCastle //int userId, int moneyCount
        } 
        public readonly int userId;
        public readonly int targetId;
        public readonly SubscriptionType subscriptionType;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
