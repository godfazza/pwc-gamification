﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class CastleInfo : DataInfo
    {
        public readonly int castleID;
        public readonly string castleName;
        public int castleLevel;
        public int eventMoney;
        public int marketMoney;
        public readonly List<PlayerData> clanMembers;
        public readonly List<BuildingInfo> buildings;
        public readonly PlayerData leader;
        public readonly List<PlayerData> coleaders;
        public readonly DateTime attackDate;
        public CastleInfo(int castleID, string castleName, int castleLevel, List<PlayerData> clanMembers,  List<BuildingInfo> buildings, List<PlayerData> coleaders, PlayerData leader, int eventMoney, int marketMoney, DateTime attackDate) : base(DataType.CastleInfo)
        {
            this.castleID = castleID;
            this.castleName = castleName;
            this.castleLevel = castleLevel;
            this.clanMembers = clanMembers;
            this.buildings = buildings;
            this.leader = leader;
            this.coleaders = coleaders;
            this.eventMoney = eventMoney;
            this.marketMoney = marketMoney;
            this.attackDate = attackDate;
        }
        public CastleInfo(int id,string castleName) : this(id,castleName,1,new List<PlayerData>(),  new List<BuildingInfo>(), new List<PlayerData>(), null, 0, 0, DateTime.Now)
        {
            
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
