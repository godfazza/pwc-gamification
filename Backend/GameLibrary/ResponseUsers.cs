﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ResponseUsers : ResponseInfo
    {
        public UsersType usersType;
        public ResponseUsers(List<PlayerData> data, UsersType usersType) : base(ResponseType.ResponseUsers)
        {
            this.players = data;
            this.usersType = usersType;
        }
        public enum UsersType
        {
            Name,
            Rating
        }
        public readonly List<PlayerData> players;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
