using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class CastleRequest:RequestInfo
    {
        public string name;
        public int number;
        public int level;
        public int introducedResources;

        public RegistrationRequest(string name, int number, int level, int introducedResources)
        {
            this.name - name;
            this.number = number;
            this.level = level;
            this.introducedResources = introducedResources;
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
