﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class PlayerData: DataInfo
    {
        public PlayerData(long Items, long WornItems, string NickName, int Rating, int Money, int Id, string Email, int castleId, string Description, bool isBanned , DateTime collectDate) : base( DataType.PlayerInfo )
        {
            this.Items = Items;
            this.WornItems = WornItems;
            this.NickName = NickName;
            this.Rating = Rating;
            this.Money = Money;
            this.Id = Id;
            this.Email = Email;
            this.castleId = castleId;
            this.Description = Description;
            this.isBanned = isBanned;
            this.collectDate = collectDate;
        }


        public PlayerData(byte[] data): base(DataType.PlayerInfo)
        {
            PlayerData a = (PlayerData)Deserialize(data);
            this.Items = a.Items;
            this.WornItems = a.WornItems;
            this.NickName = a.NickName;
            this.Rating = a.Rating;
            this.Money = a.Money;
        }
        public readonly long Items; //i-ая единица - имеется i-ый предмет
        public readonly long WornItems; //Надетые предметы
        public readonly string NickName;
        public readonly int Rating;
        public int Money;
        public readonly int Id;
        public readonly string Email;
        public readonly string Description;
        public readonly bool isBanned;
        public DateTime collectDate;
        public int castleId;
        int GetItem(int number)
        {
            long items = Items;
            while (number-- > 0)
                items /= 2;
            return (int)(items % 2);
        }
        int GetWornItem(int number)
        {
            long items = WornItems;
            while (number-- > 0)
                items /= 2;
            return (int)(items % 2);
        }

        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }

        public override string ToString()
        {
            return NickName + " Id" + Id;
        }
    }
}
