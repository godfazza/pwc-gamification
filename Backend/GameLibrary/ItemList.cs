using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    /// <summary>
    /// Класс определяющий все доступные характеристики игры
    /// </summary>
    [Serializable]
    public class Power
    {
        public Power() : this(0, 0, 0)
        {
        }

        public Power(int attack, int health, int price)
        {
            this.Attack = attack;
            this.Health = health;
            this.Price = price;
        }

        public int Attack;
        public int Health;
        public int Price;

        public void AddPower(Power powerToAdd)
        {
            this.Attack += powerToAdd.Attack;
            this.Health += powerToAdd.Health;
            this.Price += powerToAdd.Price;
        }

        public static Power operator +(Power a, Power b)
        {
            return new Power(a.Attack + b.Attack, a.Health + b.Health, a.Price + b.Price);
        }
    }

    [Serializable]
    public class ItemList
    {
        public static Item GetItemByID(int id)
        {
            foreach (var item in ItemList.Items)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }

            return null;
        }
        public static long BuyItem(int id, long code)
        {
            return code | ( 1 << id );
        }
        /// <summary>
        /// Список всех предметов игры
        /// </summary>
        public static List<Item> Items = new List<Item>
        {
            //office
            new Item(1, 1, 0, 0, ItemType.Body),
            new Item(2, 1, 0, 0, ItemType.Head),
            new Item(3, 0, 1, 0, ItemType.Weapon),

            //warrior
            new Item(4, 4, 0, 3, ItemType.Body),
            new Item(5, 2, 3, 3, ItemType.Head),
            new Item(6, 0, 3, 3, ItemType.Weapon),

            //ninja
            new Item(7, 8, 3, 8, ItemType.Body),
            new Item(9, 0, 5, 8, ItemType.Weapon),

            //king
            new Item(10, 10, 1, 16, ItemType.Body),
            new Item(11, 8, 4, 16, ItemType.Head),
            new Item(12, 8, 3, 16, ItemType.Weapon),

            //princess
            new Item(13, 3, 1, 3, ItemType.Body),
            new Item(14, 2, 2, 2, ItemType.Head),
            new Item(15, 5, 15, 20, ItemType.Weapon)
        };


        /// <summary>
        /// Выделяет из кода все предметы, записанные в нем и записывает их ID  список
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static List<long> GetIds(long code)
        {
            List<long> ans = new List<long>();
            long pow = 1;
            for (int i = 0; i <= 63; i++)
            {
                if ((code / pow) % 2 == 1)
                {
                    ans.Add(i);
                }

                pow *= 2;
            }

            return ans;
        }


        /// <summary>
        /// Возвращает обьект класса Power, в котором содержатся все характеристики
        /// </summary>
        /// <param name="code">ID надетых предметов</param>
        public static Power GetPower(long code)
        {
            Power ans = new Power();
            List<long> ids = GetIds(code);
            for (int i = 0; i < ids.Count; i++)
            {
                for (int j = 0; j < Items.Count; j++)
                {
                    if (ids[i] == Items[j].Id)
                    {
                        ans.AddPower(Items[j].ItemPower);
                    }
                }
            }

            return ans;
        }

        /// <summary>
        /// Надевает предмет
        /// </summary>
        /// <param name="id">id предмета который нужно надет</param>
        /// <param name="code">code же надетых предметов</param>
        /// <returns>Возвращает новый код надетых предметов</returns>
        public static long PutItem(int id, long code)
        {
            //Получаем все надетые предметы и находим тот, что имеет тип совпадающий
            //с необходимым, заменяем его в массиве и преобразовываем обратно в код.
            long newCode = 0;
            List<long> items = GetIds(code);
            Item itemToPut = GetItemByID(id);
            for (int i = 0; i < items.Count; i++)
            {
                if (GetItemByID((int) (items[i])).Type == itemToPut.Type)
                {
                    items[i] = id;
                }
            }

            return GetCodeByList(items);
        }

        public static long GetCodeByList(List<long> ids)
        {
            long code = 0;
            for (int i = 0; i < ids.Count; i++)
            {
                code += pow2((int) ids[i]);
            }

            return code;
        }

        public static long pow2(int n)
        {
            long ans = 1;
            for (int i = 0; i < n; i++)
                ans *= 2;
            return ans;
        }
    }
}