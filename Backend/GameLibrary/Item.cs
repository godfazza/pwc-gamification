using System.Security.Permissions;
using System.Collections.Generic;
using System;
namespace GameLibrary
{
    public enum ItemType
    {
        Head,
        Weapon,
        Body
    }
    [Serializable]
    public class Item
    {
        public Power ItemPower;
        public int Id; 
        public ItemType Type;

        public Item(int id, int health, int attack, int price, ItemType type)
        {
            Id = id; 
            Type = type;
            ItemPower = new Power {Attack = attack, Health = health, Price = price};
        }
    }
}