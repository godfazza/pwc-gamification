﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace GameLibrary
{
    [Serializable]
    public class ResponseUserEvent : ResponseInfo
    {
        public ResponseUserEvent(List<EventUserData> data) : base(ResponseType.ResponseEvents)
        {
            this.data = data;
        }
        public readonly List<EventUserData> data;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
