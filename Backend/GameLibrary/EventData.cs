﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
   public class EventData: DataInfo
    {

        public EventData(int id, DateTime start, DateTime end, string title, string description, int ratingAward, int moneyAward, List<PlayerData> admins, List<PlayerData> managers, bool isAwarded = false):base(DataType.EventInfo)
        {
            this.id = id;
            this.ratingAward = ratingAward;
            this.moneyAward = moneyAward;
            this.start = start;
            this.end = end;
            this.title = title;
            this.description = description;
            this.admins = admins;
            this.managers = managers;
            this.isAwarded = isAwarded;//обработать в БД
        }

        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
        public void AddAdmin(PlayerData admin)
        {
            if (!ExistPlayer(admins,admin))
                admins.Add(admin);
        }

        public void AddManager(PlayerData manager)
        {
            if (!ExistPlayer(managers, manager))
                managers.Add(manager);
        }

        public static bool ExistPlayer(List<PlayerData> list, PlayerData player)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Id == player.Id)
                    return true;
            }
            return false;
        }

        public readonly List<PlayerData> admins;
        public readonly List<PlayerData> managers;
        public readonly string title;
        public readonly string description;
        public readonly int ratingAward;
        public readonly int id;
        public readonly int moneyAward;
        public readonly DateTime start;
        public readonly DateTime end;
        public readonly bool isAwarded;
    }
}
