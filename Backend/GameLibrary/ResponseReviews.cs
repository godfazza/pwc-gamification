﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ResponseReviews : ResponseInfo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <param name="rrt">выбрать из списка</param>
        public ResponseReviews(List<ReviewData> list, ResponseReviewsType responseReviewsType) : base(ResponseType.ResponseReviews)
        { 
            this.reviews = list;
            this.responseReviewsType = responseReviewsType;
        }

        public enum ResponseReviewsType
        {
            EventReviews,
            CastleReviews
        }

        public readonly ResponseReviewsType responseReviewsType;
        public readonly List<ReviewData> reviews;

        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
