using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class CastleRequest:RequestInfo
    {
        public int castleID;
        public string castleName;
        public int castleLevel;
        public RegistrationRequest(int castleID, string name, int castleLevel)
        {
            this.castleID = castleID;
            this.castleName = castleName;
            this.castleLevel = castleLevel;
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
