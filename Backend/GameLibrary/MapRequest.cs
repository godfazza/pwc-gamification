using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class CastleRequest:RequestInfo
    {
        public string worldMap;
        public string castlesMap;

        public RegistrationRequest(string worldMap, string castlesMap)
        {
            this.worldMap = worldMap;
            this.castlesMap = castlesMap;
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
