﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class ResponseUpdate : ResponseInfo
    {
        public readonly ResponseType responseType;
       
        public ResponseUpdate(ResponseInfo updatingData, ResponseUpdateType responseUpdateType) : base( ResponseType.ResponseUpdate )
        {
            this.updatingData = updatingData;
            this.responseUpdateType = responseUpdateType;
        }
        public readonly ResponseInfo updatingData;
        public readonly ResponseUpdateType responseUpdateType;
        public enum ResponseUpdateType
        {
            UpdatePlayer,
            UpdateCastleReviews,
            UpdateEventReviews
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
    }
}
