﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class BuildingInfo : DataInfo
    {
        public string name;
        public int number;
        public int level;
        public int introducedResources;

        public BuildingInfo(string name, int number, int level, int introducedResources) : base( DataType.BuildingInfo )
        {
            this.name = name;
            this.number = number;
            this.level = level;
            this.introducedResources = introducedResources;
        }
        public BuildingInfo(string name, int number) : this( name, number, 1, 0 ) { }

        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
    }
}
