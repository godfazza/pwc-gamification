﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace GameLibrary
{
    [Serializable]
    public class ResponseMainInfo : ResponseInfo
    {
        public ResponseMainInfo(DateTime dt, string gv, bool atg = true, string reason = "") : base( ResponseType.ResponseMainInfo )
        {
            serverDate = dt;
            gameVersion = gv;
            accessToGame = atg;
            this.reason = reason;
        }

        public readonly DateTime serverDate;
        public readonly string gameVersion;
        public readonly bool accessToGame;
        public readonly string reason;

        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
