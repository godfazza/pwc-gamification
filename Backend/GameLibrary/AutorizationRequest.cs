using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class AutorizationRequest:RequestInfo
    {
        public string Password;
        public string EMail;
        public AutorizationRequest(string EMail, string Password) : base(RequestType.Autorization)
        {
            this.EMail = EMail;
            this.Password = Password;
        }
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, this);
            return stream.ToArray();
        }
    }
}
