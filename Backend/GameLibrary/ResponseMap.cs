﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace GameLibrary
{
    [Serializable]
    public class ResponseMap : ResponseInfo
    {
        public ResponseMap(MapInfo map) : base( ResponseType.ResponseMap )
        {
            this.map = map;
        }
        public readonly MapInfo map;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
    }
}
