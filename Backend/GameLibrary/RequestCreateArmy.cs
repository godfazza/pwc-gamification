﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameLibrary
{
    [Serializable]
    public class RequestCreateArmy : RequestInfo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"> Тот, кто создает рейд</param>
        /// <param name="myCastleId"> Замок из которого рейд</param>
        /// <param name="targetCastle"> Замок на который рейд</param>
        /// <param name="soldiers"> ID солдат, взятых в рейд</param>
        public RequestCreateArmy(int userId, int myCastleId, int targetCastle, List<int> soldiers) : base( RequestType.CreateArmy )
        {
            this.userId = userId;
            this.myCastleId = myCastleId;
            this.targetCastle = targetCastle;
            this.soldiers = soldiers;
        }


        public readonly int userId;
        public readonly int myCastleId;
        public readonly int targetCastle;
        public readonly List<int> soldiers;
        public override byte[] Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize( stream, this );
            return stream.ToArray();
        }
    }
}
