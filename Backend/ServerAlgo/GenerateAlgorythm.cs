using System;
using System.Collections.Generic;

public class GenerateAlgorithm
{
    public Dictionary<int, string> castles;

    public int fieldWidth, fieldHeight;

    public MapCoordinateController mcc;

    int seed;
    System.Random randomBySeed;

    public GenerateAlgorithm(int fieldWidth, int fieldHeight, DateTime dateTime)
    {
        this.fieldWidth = fieldWidth;
        this.fieldHeight = fieldHeight;
        this.mcc = new MapCoordinateController(fieldHeight, fieldWidth);
        seed = GetSeedByDateTime(dateTime);
        this.randomBySeed = new System.Random(seed);
    }

    public int NextRandom(int min, int max)
    {
        return min + randomBySeed.Next() % (max - min);
    }

    public int GetSeedByDateTime(DateTime dateTime)
    {
        return dateTime.Year * 12 + dateTime.Month;
    }

    public string GenerateMap()
    {
        string map = GenerateEmptyMap();

        map = GenerateSea(map, 4, 97);

        map = GenerateIslands(map, 98);

        map = GenerateDesert(map, 6, 98);

        map = GenerateForest(map, 2, 96);

        map = GenerateHills(map, 90);

        map = GenerateWheat(map, 98);

        map = GenerateDesertHills(map, 90);

        map = GenerateOasises(map, 97);

        return map;
    }

    string GenerateEmptyMap()
    {
        string map = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                map = map + "0";
            }
            map = map + "$";
        }

        return map;
    }

    string GenerateSea(string map, int level, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count)
                {
                    newMap += "2";
                }
                else
                {
                    newMap = newMap + map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return RaiseSeaLevel(newMap, level);
    }

    string RaiseSeaLevel(string map, int level)
    {
        if (level > 0)
        {
            string newMap = "$";

            for (int i = 0; i < fieldWidth; i++)
            {
                for (int j = 0; j < fieldHeight; j++)
                {
                    if (map[mcc.GetThis(i, j)] != '0')
                    {
                        newMap = newMap + map[mcc.GetThis(i, j)];
                    }
                    else
                    {
                        int rand = NextRandom(0, 2);

                        if (map[mcc.GetDown(i, j)] == '2')
                            rand++;
                        if (map[mcc.GetUp(i, j)] == '2')
                            rand++;
                        if (i > 0 && map[mcc.GetLeft(i, j)] == '2')
                            rand++;
                        if (i < (fieldWidth - 1) && map[mcc.GetRight(i, j)] == '2')
                            rand++;

                        if (rand > 1)
                        {
                            newMap = newMap + "2";
                        }
                        else
                        {
                            newMap = newMap + map[mcc.GetThis(i, j)];
                        }
                    }
                }
                newMap = newMap + "$";
            }
            return RaiseSeaLevel(newMap, level - 1);
        }
        else
        {
            return map;
        }
    }

    string GenerateDesert(string map, int level, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count && map[mcc.GetThis(i, j)] == '0')
                {
                    newMap += "1";
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return RaiseDesertSize(newMap, level);
    }

    string RaiseDesertSize(string map, int level)
    {
        if (level > 0)
        {
            string newMap = "$";

            for (int i = 0; i < fieldWidth; i++)
            {
                for (int j = 0; j < fieldHeight; j++)
                {
                    if (map[mcc.GetThis(i, j)] != '0')
                    {
                        newMap += map[mcc.GetThis(i, j)];
                    }
                    else
                    {
                        int rand = NextRandom(0, 2);

                        if (map[mcc.GetUp(i, j)] == '1')
                            rand = rand + 1;
                        if (map[mcc.GetDown(i, j)] == '1')
                            rand = rand + 1;
                        if (i > 0 && map[mcc.GetLeft(i, j)] == '1')
                            rand = rand + 1;
                        if (i < (fieldHeight - 1) && map[mcc.GetRight(i, j)] == '1')
                            rand = rand + 1;
                        if (newMap[mcc.GetUp(i, j)] == '2')
                            rand = rand - 2;
                        if (map[mcc.GetDown(i, j)] == '2')
                            rand = rand - 2;
                        if (i > 0 && newMap[mcc.GetLeft(i, j)] == '2')
                            rand = rand - 2;
                        if (i < (fieldHeight - 1) && map[mcc.GetRight(i, j)] == '2')
                            rand = rand - 2;
                        if (rand > 1)
                        {
                            newMap += "1";
                        }
                        else
                        {
                            newMap += map[mcc.GetThis(i, j)];
                        }
                    }
                }
                newMap = newMap + "$";
            }
            return RaiseDesertSize(newMap, level - 1);
        }
        else
        {
            return map;
        }
    }

    string GenerateForest(string map, int level, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count && map[mcc.GetThis(i, j)] == '0')
                {
                    newMap += "3";
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return RaiseForestSize(newMap, level);
    }

    string RaiseForestSize(string map, int level)
    {
        if (level > 0)
        {
            string newMap = "$";

            for (int i = 0; i < fieldWidth; i++)
            {
                for (int j = 0; j < fieldHeight; j++)
                {
                    if (map[mcc.GetThis(i, j)] != '0')
                    {
                        newMap += map[mcc.GetThis(i, j)];
                    }
                    else
                    {
                        int rand = NextRandom(0, 2);

                        if (map[mcc.GetUp(i, j)] == '3')
                            rand = rand + 1;
                        if (map[mcc.GetDown(i, j)] == '3')
                            rand = rand + 1;
                        if (i > 0 && map[mcc.GetLeft(i, j)] == '3')
                            rand = rand + 1;
                        if (i < (fieldHeight - 1) && map[mcc.GetRight(i, j)] == '3')
                            rand = rand + 1;
                        if (map[mcc.GetUp(i, j)] == '1')
                            rand = rand - 2;
                        if (map[mcc.GetDown(i, j)] == '1')
                            rand = rand - 2;
                        if (i > 0 && map[mcc.GetLeft(i, j)] == '1')
                            rand = rand - 2;
                        if (i < (fieldHeight - 1) && map[mcc.GetRight(i, j)] == '1')
                            rand = rand - 2;
                        if (rand > 1)
                        {
                            newMap += "3";
                        }
                        else
                        {
                            newMap += map[mcc.GetThis(i, j)];
                        }
                    }
                }
                newMap = newMap + "$";
            }
            return RaiseForestSize(newMap, level - 1);
        }
        else
        {
            return map;
        }
    }

    string GenerateIslands(string map, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count)
                {
                    if (i > 0 && i < (fieldWidth - 1) && map[mcc.GetThis(i, j)] == '2' && map[mcc.GetUp(i, j)] == '2' && map[mcc.GetDown(i, j)] == '2' &&
                        map[mcc.GetLeft(i, j)] == '2' && map[mcc.GetRight(i, j)] == '2' && map[mcc.GetDownRight(i, j)] == '2' && map[mcc.GetUpRight(i, j)] == '2' &&
                        map[mcc.GetDownLeft(i, j)] == '2' && map[mcc.GetUpLeft(i, j)] == '2')
                        newMap += "0";
                    else
                        newMap += map[mcc.GetThis(i, j)];
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return newMap;
    }

    string GenerateHills(string map, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count)
                {
                    if ((map[mcc.GetThis(i, j)] == '0') && ((i > 0 && i < (fieldWidth - 1) && newMap[mcc.GetLeft(i, j)] != '4') && (j > 0 && j < (fieldHeight - 1) && newMap[mcc.GetUp(i, j)] != '4')))
                        newMap += "4";
                    else
                        newMap += map[mcc.GetThis(i, j)];
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return newMap;
    }

    string GenerateDesertHills(string map, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count)
                {
                    if ((map[mcc.GetThis(i, j)] == '1') && ((i > 0 && i < (fieldWidth - 1) && newMap[mcc.GetLeft(i, j)] != '6') && (j > 0 && j < (fieldHeight - 1) && newMap[mcc.GetUp(i, j)] != '6'))
                        && map[mcc.GetDown(i, j)] == '1' && map[mcc.GetUp(i, j)] == '1' && map[mcc.GetRight(i, j)] == '1' && map[mcc.GetLeft(i, j)] == '1')
                        newMap += "6";
                    else
                        newMap += map[mcc.GetThis(i, j)];
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return newMap;
    }

    string GenerateWheat(string map, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count)
                {
                    if (map[mcc.GetThis(i, j)] == '0')
                        newMap += "5";
                    else
                        newMap += map[mcc.GetThis(i, j)];
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return newMap;
    }

    string GenerateOasises(string map, int count)
    {
        string newMap = "$";

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int rand = NextRandom(0, 100);

                if (rand > count)
                {
                    if ((map[mcc.GetThis(i, j)] == '1') && ((i > 0 && i < (fieldWidth - 1) && newMap[mcc.GetLeft(i, j)] != '7') && (j > 0 && j < (fieldHeight - 1) && newMap[mcc.GetUp(i, j)] != '7'))
                        && map[mcc.GetDown(i, j)] == '1' && map[mcc.GetUp(i, j)] == '1' && map[mcc.GetRight(i, j)] == '1' && map[mcc.GetLeft(i, j)] == '1')
                        newMap += "7";
                    else
                        newMap += map[mcc.GetThis(i, j)];
                }
                else
                {
                    newMap += map[mcc.GetThis(i, j)];
                }
            }
            newMap = newMap + "$";
        }

        return newMap;
    }

    public string GenerateCastles(string map)
    {
        string castlesMap = "$";
        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                if (map[mcc.GetThis(i, j)] == '0' || map[mcc.GetThis(i, j)] == '1')
                {
                    int rand = NextRandom(0, 100);
                    if (rand > 90 && i > 1 && j > 1 && i < fieldWidth - 2 && j < fieldHeight - 2 && castlesMap[i * (fieldHeight + 1) + j] == '0' && castlesMap[i * (fieldHeight + 1) + j - 1] == '0' &&
                        castlesMap[(i - 1) * (fieldHeight + 1) + j - 1] == '0' && castlesMap[(i - 1) * (fieldHeight + 1) + j] == '0' && castlesMap[(i - 1) * (fieldHeight + 1) + j + 1] == '0' &&
                        castlesMap[(i - 1) * (fieldHeight + 1) + j + 2] == '0' && castlesMap[(i - 1) * (fieldHeight + 1) + j + 3] == '0' &&
                        castlesMap[(i - 2) * (fieldHeight + 1) + j - 1] == '0' && castlesMap[(i - 2) * (fieldHeight + 1) + j] == '0' && castlesMap[(i - 2) * (fieldHeight + 1) + j + 1] == '0' &&
                        castlesMap[(i - 2) * (fieldHeight + 1) + j + 2] == '0' && castlesMap[(i - 2) * (fieldHeight + 1) + j + 3] == '0')
                        castlesMap += "1";
                    else
                        castlesMap += "0";
                }
                else
                    castlesMap += "0";
            }
            castlesMap = castlesMap + "$";
        }

        castles = GenerateCastlesNames(GetCastlesId(castlesMap));

        return castlesMap;
    }

    public Dictionary<int, string> GenerateCastlesNames(List<int> castlesId)
    {
        Dictionary<int, string> castlesNames = new Dictionary<int, string>();

        foreach(int id in castlesId)
        {
            castlesNames.Add(id, GenerateCastleName());
        }

        return castlesNames;
    }

    public string GenerateCastleName()
    {
        string[] firstName = { "Green", "Bloody", "Black", "Pink", "Gray", "Sad", "Warm", "New", "Dead", "Glorious", "Sky", "Cold", "Dark", "Lucky", "Quiet", "Distant", "Rich", "Small", "Tiny", "Wonderful", "Weird" };
        string[] secondName = { "Castle", "Fortress", "Stronghold", "Citadel", "Palace", "Town", "Fort" };

        return firstName[NextRandom(0, firstName.Length)] + " " + secondName[NextRandom(0, secondName.Length)];
    }

    public List<int> GetCastlesId(string castlesMap)
    {
        List<int> castlesId = new List<int>();

        for (int i = 0; i < fieldWidth; i++)
        {
            for (int j = 0; j < fieldHeight; j++)
            {
                int castleIndex = (int)(castlesMap[mcc.GetThis(i, j)] - '0');

                if (castleIndex == 1)
                {
                    castlesId.Add(i * fieldHeight + j);
                }
            }
        }

        return castlesId;
    }

    public Dictionary<int, string> GetCastles()
    {
        return castles;
    }
}

public class MapCoordinateController
{
    int fieldHeight;
    int fieldWidth;
    public MapCoordinateController(int fieldHeight, int fieldWidth)
    {
        this.fieldHeight = fieldHeight;
        this.fieldWidth = fieldWidth;
    }

    public int GetThis(int x, int y)
    {
        return x * (fieldHeight + 1) + y + 1;
    }

    public int GetRight(int x, int y)
    {
        return (x + 1) * (fieldHeight + 1) + y + 1;
    }

    public int GetLeft(int x, int y)
    {
        return (x - 1) * (fieldHeight + 1) + y + 1;
    }

    public int GetUp(int x, int y)
    {
        return x * (fieldHeight + 1) + y;
    }

    public int GetDown(int x, int y)
    {
        return x * (fieldHeight + 1) + y + 2;
    }

    public int GetUpRight(int x, int y)
    {
        return (x + 1) * (fieldHeight + 1) + y;
    }

    public int GetUpLeft(int x, int y)
    {
        return (x - 1) * (fieldHeight + 1) + y;
    }

    public int GetDownRight(int x, int y)
    {
        return (x + 1) * (fieldHeight + 1) + y + 2;
    }

    public int GetDownLeft(int x, int y)
    {
        return (x - 1) * (fieldHeight + 1) + y + 2;
    }

}
