﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using ServerLibrary;
using GameLibrary;
using System.Collections.Generic;

namespace ServerPwC
{
    
    class Program
    {
        static List<ClientObject> onlinePlayers = new List<ClientObject>();
        static int port = 80;
        static TcpListener listener;
        static DataBaseConnector dataBaseConnector;
        static DataBaseConnectorGame dataBaseConnectorGame;
        static DataBaseUpdate dataBaseUpdate;
        static string IP = "10.0.0.4";
        static ServerUpdater serverUpdater;
        static void Main(string[] args)
        {
            Console.Write("IP: ");
            //IP = Console.ReadLine();
            Console.Write( "port: " );
            //port = int.Parse(Console.ReadLine());
            try
            {
                listener = new TcpListener(IPAddress.Parse(IP), port);
                
                
                dataBaseConnector = new DataBaseConnector();
                dataBaseConnectorGame = new DataBaseConnectorGame();
                dataBaseUpdate = new DataBaseUpdate();
                listener.Start();
                //Console.WriteLine($"IP = {IP}; port = {port};");
                Console.WriteLine("Waiting for connections...");
                serverUpdater = new ServerUpdater( dataBaseConnector, onlinePlayers, dataBaseUpdate );
                Thread updater = new Thread( new ThreadStart( serverUpdater.Process ) );
                
                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    ClientObject clientObject = new ClientObject(client, dataBaseConnector, onlinePlayers, dataBaseConnectorGame, dataBaseUpdate);
                    onlinePlayers.Add( clientObject );
                    Console.WriteLine("Connect " + client.ToString());
                    // создаем новый поток для обслуживания нового клиента
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                    //Console.WriteLine("Отправил " + test.SerializeObj().ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (listener != null)
                    listener.Stop();
                Console.ReadLine();
            }
        }
    }
}
