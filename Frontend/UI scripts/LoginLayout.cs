﻿using System.Collections;
using System.Collections.Generic;
using UI_scripts;
using UnityEngine;
using UnityEngine.UI;

public class LoginLayout : MonoBehaviour
{
    public InputField PasswordField;
    public InputField LoginField;
    public Text TextForMessages;

    public string Login;
    public string Password;

    private void Start()
    {
        Links.LoginLayout = this;
    }

    public void UpdateInputs()
    {
        Login = LoginField.text;
        Password = PasswordField.text;
    }
}