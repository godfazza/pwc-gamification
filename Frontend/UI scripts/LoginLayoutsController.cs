﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using ClientServerScripts;
using UI_scripts;
using UnityEngine;

public class LoginLayoutsController : MonoBehaviour
{
    public GameObject LoginLayout;
    public GameObject LoadingLayout;
    public GameObject SignupLayout;

    public void LogInPressed()
    {
        ShowLoadingLayout();
        HideLoginLayout();

        //new Thread(SendEnteredFields).Start();
        StartCoroutine(SendEnteredFields());
    }

    public IEnumerator SendEnteredFields()
    {
        RequestResult result = new RequestResult();
        StartCoroutine(Links.RequestController.TryLogIn(Links.LoginLayout.Login, Links.LoginLayout.Password, result));

        yield return new WaitWhile(() => { return result.IsCalculating; });

        if (result.Success)
        {
            Links.MainMenuController.ShowMainMenuLayout();
            HideLoadingLayout();
            HideLoginLayout();
        }
        else
        {
            ShowLoginLayout();
            HideLoadingLayout();


            Links.LoginLayout.TextForMessages.text = result.UserMessage;
        }

        yield return null;
    }


    public void CreateAccountPressed()
    {
        string login = Links.SignupLayout.Login;
        string password = Links.SignupLayout.Password;
        string confirmPassword = Links.SignupLayout.ConfirmedPassword;
        string message = "";
        
        if (!FirstCheck(login, password, confirmPassword, ref message))
        {
            Links.SignupLayout.TextForMessages.text = message;
            return;
        }

        ShowLoadingLayout();
        HideSignupLayout();
        
        StartCoroutine(RegisterNewUser());
    }

    bool FirstCheck(string login, string password, string confirmPassword, ref string message)
    {
        if (password != confirmPassword)
        {
            message = "passwords do not match";
            return false;
        }

        if (password.Length < 3)
        {
            message = "password should have at least 3 characters";
            return false;
        }
        
        return true;
    }

    
    public IEnumerator RegisterNewUser()
    {
        RequestResult result = new RequestResult();
        StartCoroutine(Links.RequestController.TryLogIn(Links.SignupLayout.Login, Links.SignupLayout.Password, result));

        yield return new WaitWhile(() => result.IsCalculating);

        if (result.Success)
        {
            Links.MainMenuController.ShowMainMenuLayout();
            HideLoadingLayout();
            HideLoginLayout();
            HideSignupLayout();
        }
        else
        {
            ShowSignupLayout();
            HideLoadingLayout();
            Links.SignupLayout.TextForMessages.text = result.UserMessage;
        }

        yield return null;
    }



    public void GoToSignUpButtonPressed()
    {
        ShowSignupLayout();
        HideLoginLayout();
    }
    
    

    public void HideLoginLayout()
    {
        LoginLayout.SetActive(false);
    }

    public void ShowLoginLayout()
    {
        LoginLayout.SetActive(true);
    }

    public void HideLoadingLayout()
    {
        LoadingLayout.SetActive(false);
    }

    public void ShowLoadingLayout()
    {
        LoadingLayout.SetActive(true);
    }


    public void HideSignupLayout()
    {
        SignupLayout.SetActive(false);
    }

    public void ShowSignupLayout()
    {
        SignupLayout.SetActive(true);
    }
}